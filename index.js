const config = require('./config')
const puppeteerIterator = require('./utils/puppeteer-iterator')
const resizeWindow = require('./utils/resize-window')
const screenShot = require('./utils/screen-shot')
const Url = require('url-parse')
const STATUS = require('./constants').status

/**
 * @extends {puppeteerIterator}
 * @type {puppeteerIterator}
 */
const runner = Object.create(puppeteerIterator)

/**
 * setUp
 * Either sets the testUrls from command line, or from the fractal start page
 * @this {puppeteerIterator}
 * @returns null
 */
runner.setUp = async function () {
  await puppeteerIterator.setUp.call(this)
  if (this.config.testUrls) {
    this.setUpCompareVs()
    return this.config.testUrls
  }
  const testUrls = await this.page.$$eval('.Tree-entityLink', (hrefs) => hrefs.map((a) => a.href))
  this.config.testUrls = testUrls.map(url => url.replace('/detail/', '/preview/'))
}
/**
 * afterSingleTest
 * @super
 * @param testUrl: String
 * @returns {Promise<void>}
 */
runner.afterSingleTest = async function (testUrl) {
  puppeteerIterator.afterSingleTest.call(this, testUrl)
  this.logger.console(`finished ${testUrl}`)
}

/**
 * executeSingleTest
 * @super
 * @param screenSize: Array
 * ↳ height: Number
 * ↳ width: Number
 * @param url: String
 * @returns {Promise<void>}
 */
runner.executeSingleTest = async function (screenSize, url) {
  await puppeteerIterator.executeSingleTest.call(this, screenSize, url)

  await this.page.addStyleTag({ path: './fractal-puppeteer.css' })

  await this.page.waitFor(1000)

  await resizeWindow(this.page, this.browser, screenSize)

  const imageData = await this.createScreenShot(screenSize, url)

  if (this.testBranch !== 'master') {
    await this.compare(imageData, url)
  }
}

/**
 * createScreenShot
 * @param {object} screenSize: {height: number, width: number}
 * @param {string} url
 * @param {number=} baseImageIndex
 * @return {Promise<ImageData>}
 */
runner.createScreenShot = async function (screenSize, url, baseImageIndex = 0) {
  if (this.config.screenShot.capture) {
    try {
      /** @type {ImageData} */
      return await screenShot({
        baseImagePath: this.config.baseImagePath[baseImageIndex],
        page: this.page,
        screenSize,
        url
      })
    } catch (error) {
      this.logger.log({
        name: STATUS.ERROR,
        context: 'createScreenShot',
        url,
        screenSize,
        message: error.toString()
      })
      throw error
    }
  }
}

runner.getMasterImagePath = function (imageData) {
  if (this.config.compareVs) {
    // we need to check if this is the second image,
    // and if so, alter the path so it can be resolved.?
  } else {
    return puppeteerIterator.getMasterImagePath.call(this, imageData)
  }
}
/**
 * compare
 * passed in imageData is process and compared to the equivalent image in master
 * if it exists.
 * @param imageData: Object
 *  ↳ baseName: String - url representation of fileName, eg localhost!components!preview!about-us.png
 *  ↳ height: Number
 *  ↳ path: String - relative uri to image
 *  ↳ sizeDir: String - e.g. 1400x1080
 *  ↳ width: Number
 * @param url: String
 * @returns {Promise<void>}
 */
runner.compare = async function (imageData, url) {
  if (imageData && this.screenShot.compare) {
    const context = 'compare'
    let response = null

    try {
      response = await this.compareImages.testMatch(
        imageData.path,
        this.getMasterImagePath(imageData),
        this.testBranch
      )
    } catch (error) {
      throw error
    } finally {
      if (response) {
        this.logger.log({
          name: STATUS.ERROR,
          url,
          context,
          message: response
        })
      }
    }
  }
}

runner.setUpCompareVs = function () {
  if (this.config.compareVs) {
    this.config.testUrls.forEach((testUrl, index) => {
      const url = new Url(testUrl)
      this.config.baseImagePath[index] = `./images/${url.hostname}`
    })
  }
}

runner.init(config)
