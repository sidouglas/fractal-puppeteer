const yargs = require('yargs')
const gitBranch = Object.create(require('./utils/git-branch'))
gitBranch.init('/Users/simon/Documents/nrl.pr', './images/')

const argv = yargs
  .usage('Fractal Runner\n\nUsage: $0 [options]')
  .help('help').alias('help', 'h')
  .version('version', '1.0').alias('version', 'V')
  .array(['urls', 'vs'])
  .options({
    urls: {
      alias: 'u',
      description: 'urls eg: --u foo bar qux',
      requiresArg: false,
      required: false
    },
    capture: {
      alias: 'c',
      default: true,
      description: 'capture — captures target branch and saves image',
      required: false,
      requiresArg: false
    },
    compare: {
      alias: 'p',
      default: true,
      description: 'compare — compare target branch with saved images from master.\nOutputs differences in diff directory',
      required: false,
      requiresArg: false
    },
    vs: {
      alias: 'v',
      description: 'TODO Compares two urls',
      required: false,
      requiresArg: false
    }
  }).argv

module.exports = {
  baseImagePath: [ gitBranch.path ],
  startUrl: 'http://localhost:3000',
  testBranch: gitBranch.branch,
  screenShot: {
    capture: argv.capture,
    compare: argv.compare
  },
  screenSizes: [
    {
      width: 320,
      height: 480
    },
    {
      width: 600,
      height: 960
    },
    {
      width: 960,
      height: 1280
    },
    {
      width: 1400,
      height: 2048
    }
  ],
  testUrls: argv.urls || argv.vs,
  compareVs: !!(argv.vs && argv.vs.length === 2)
}
