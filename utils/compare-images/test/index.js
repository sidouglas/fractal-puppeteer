const compareImages = require('../index')

const tests = [
  {
    a: './feature/nrlcom-3455-mc-audio/1400x2048/localhost*3000!components!preview!about-us.png',
    b: './master/1400x2048/localhost*3000!components!preview!about-us.png',
    c: 'feature/NRLCOM-3455-mc-audio'
  },
  {
    a: './feature/nrlcom-3455-mc-audio/960x1280/localhost*3000!components!preview!about-us.png',
    b: './master/960x1280/localhost*3000!components!preview!about-us.png',
    c: 'feature/NRLCOM-3455-mc-audio'
  },
  {
    a: './feature/nrlcom-3455-mc-audio/600x960/localhost*3000!components!preview!about-us.png',
    b: './master/600x960/localhost*3000!components!preview!about-us.png',
    c: 'feature/NRLCOM-3455-mc-audio'
  },
  {
    a: './feature/nrlcom-3455-mc-audio/320x480/localhost*3000!components!preview!about-us.png',
    b: './master/320x480/localhost*3000!components!preview!about-us.png',
    c: 'feature/NRLCOM-3455-mc-audio'
  }
];

(async function () {
  for await (const test of tests) {
    const g = Object.create(compareImages).init({ diffDir: './diff' })
    await g.testMatch(
      test.a,
      test.b,
      test.c
    )
  }
}())
