const fs = require('fs-extra')
const path = require('path')
const pixelmatch = require('pixelmatch')
const snakeCase = require('snake-case')
const PNG = require('pngjs').PNG
const STATUS = require('../../constants').status

const defaults = {
  diffDir: './images/diff',
  pixelmatchConfig: {
    threshold: 0.1,
    includeAA: false
  }
}

const compareImages = {
  init (config) {
    Object.assign(this, defaults, config)
    return this
  },
  async checkPathExists (testPaths) {
    this.status = STATUS.PASS
    for await (const key of Object.keys(testPaths)) {
      try {
        this[key] = await this.readImage(testPaths[key])
      } catch (error) {
        this.status = error.toString()
        return {
          name: this.status,
          message: `${this.status} ${testPaths[key]} does not exist for comparison`,
          type: key,
          path: testPaths[key]
        }
      }
    }
    return this.status
  },
  /**
   * readImage
   * @param imagePath:String
   * @returns Promise:<Stream>
   */
  async readImage (imagePath) {
    return new Promise((resolve, reject) => {
      if (!fs.existsSync(imagePath)) {
        return reject(STATUS.NOT_FOUND)
      }
      return fs.createReadStream(imagePath).pipe(new PNG())
        .on('parsed', function () {
          resolve(this)
        })
        .on('error', () => reject(STATUS.UNREADABLE_FILE))
    })
  },
  /**
   * writeImage
   * @param filePath:String
   @param image:<ImageData>
   */
  async writeImage (filePath, image) {
    fs.ensureDirSync(path.dirname(filePath))
    return new Promise((resolve) => image.pack().pipe(fs.createWriteStream(filePath)).on('finish', resolve))
  },
  /**
   * testMatch
   * @param testPath:String
   * @param masterPath:String
   * @param branchName:String
   * @return Object
   */
  async testMatch (testPath, masterPath, branchName) {
    let diffPath = null

    const pathChecks = await this.checkPathExists({
      testImage: testPath,
      masterImage: masterPath
    })

    if (this.status !== STATUS.PASS) {
      return pathChecks
    }

    const { width, height } = this.testImage
    const diff = new PNG({ width, height })

    // img1, img2, output, width, height, options
    // console.log({
    //   a: this.testImage.data,
    //   b: this.masterImage.data,
    //   c: branchName
    // });

    const hasDifference = await pixelmatch(
      this.testImage.data, // test image - Buffer
      this.masterImage.data, // master image - Buffer
      diff.data, // diff path
      this.testImage.width,
      this.testImage.height,
      this.pixelmatchConfig
    )

    if (hasDifference) {
      this.status = STATUS.FAIL
      const baseName = path.basename(testPath, '.png')
      const branch = snakeCase(branchName.toLowerCase())
      diffPath = `${this.diffDir}/${baseName}.${width}.${height}.${branch}.png`

      await this.writeImage(diffPath, diff)
    }

    return {
      diffPath,
      height,
      masterPath,
      name: this.status,
      pixelMatch: hasDifference,
      testPath,
      width
    }
  }
}

module.exports = compareImages
