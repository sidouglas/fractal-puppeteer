// https://github.com/Microsoft/TypeScript/wiki/JSDoc-support-in-JavaScript
const mergeOnlyValues = require('./merge-only-values')
const log = require('./log')
const path = require('path')
const puppeteer = require('puppeteer')
const STATUS = require('../constants').status

let compareImages = require('./compare-images/')

/**
 * @typedef {defaults} ScreenSize
 * @type {{ width: number, height: number }}
 * */
/**
 * @typedef {defaults} ScreenSizes
 * @type {Array<ScreenSize>}
 */
const screenSizes = []

/**
 * @typedef {defaults} ConfigDefaults
 * @type {{testUrls: null, startUrl: string, screenSizes: Array, launchConfig: {headless: boolean, args: string[], ignoreHTTPSErrors: boolean}, baseImagePath: string, compareImageConfig: {}, compareVs: boolean, goToOptions: {waitUntil: string, timeout: number}, diffImagePath: string, compareImageMasterPath: string}}
 */
const defaults = {
  baseImagePath: '.',
  compareImageConfig: {},
  compareVs: false,
  diffImagePath: './images/diff',
  compareImageMasterPath: './images/master',
  launchConfig: {
    headless: true,
    args: [
      '--window-size=640,480',
      '--no-sandbox',
      '--disable-setuid-sandbox'
    ],
    ignoreHTTPSErrors: true
  },
  goToOptions: {
    waitUntil: 'networkidle2',
    timeout: 5000
  },
  screenSizes,
  startUrl: '',
  testUrls: null
}

/**
 * @typedef {Object} puppeteerIterator
 * @type {{init(Object): Promise<void>, getMasterImagePath(*): string, afterSingleTest(...[*]): Promise<void>, executeSingleTest(Object, String): (!Promise<!Object|undefined>|*), goTo(String): Promise<void>, setUp(): Promise<void>, setPageListener(*=, ...[*]): void, beforeSingleTest(...[*]): Promise<void>, config: {}, iterate(): Promise<void>}}
 */
module.exports = {
  config: {},
  async afterSingleTest (...args) {
    this.setPageListener('error', ...args)
    this.setPageListener('pageerror', ...args)
  },
  async beforeSingleTest (...args) {
    this.setPageListener('error', ...args)
    this.setPageListener('pageerror', ...args)
  },
  /**
   * Adds a puppeteer css hook on the tested document itself
   * @param {Object} screenSize {{ width: number, height: number }}
   * @param {String} url
   * @returns {!Promise<!Object|undefined>|*}
   */
  executeSingleTest (screenSize, url) {
    const baseName = path.basename(url).toLowerCase()

    this.compareImages = Object.create(compareImages).init(this.config.compareImageConfig)

    return this.page.$eval('body', (body, baseName) => {
      body.classList.add('puppeteer')
      body.classList.add(`puppeteer--${baseName}`)
    })
  },
  /**
   * @param {String} url
   * @returns {Promise<void>}
   */
  async goTo (url) {
    await this.page.goto(url, this.config.goToOptions)
  },
  /**
   * init
   * @param {Object} config Config
   * @returns {Promise<void>}
   */
  async init (config) {
    try {
      this.config = mergeOnlyValues(defaults, config)
      await this.setUp()
      await this.goTo(this.config.startUrl)
      await this.iterate()
    } catch (error) {
      this.logger.log({
        name: STATUS.ERROR,
        context: 'init',
        message: error.toString()
      })
    } finally {
      process.exit()
    }
  },
  /**
   * iterate
   * Runs over each testUrl and every screenSize
   * @returns {Promise<void>}
   */
  async iterate () {
    if (!this.config.testUrls || this.config.testUrls.length === 0) {
      throw new Error('testUrls has nothing to iterate from setUp().')
    }
    for (let testUrl of this.config.testUrls) {
      try {
        await this.goTo(testUrl)
      } catch (error) {
        this.logger.log({
          context: 'iterate',
          message: error.toString(),
          name: STATUS.TIMEOUT,
          url: testUrl
        })
        continue
      }

      await this.beforeSingleTest(testUrl)

      for await (let screenSize of this.config.screenSizes) {
        try {
          await this.executeSingleTest(screenSize, testUrl)
        } catch (error) {
          this.logger.log({
            context: 'executeSingleTest',
            message: error.toString(),
            name: STATUS.TIMEOUT,
            url: testUrl
          })
          throw error
        }
      }

      await this.afterSingleTest(testUrl)
    }
  },
  /**
   *setPageListener
   * @param {String }listenerName
   * @param args
   * @returns null
   */
  setPageListener (listenerName, ...args) {
    let addOrRemove = 'on'
    this._pageListener = this._pageListener || {}
    if (this._pageListener[listenerName]) {
      addOrRemove = 'removeListener'
      delete this._pageListener[listenerName]
    } else {
      this._pageListener[listenerName] = addOrRemove
    }
    this.page[addOrRemove](listenerName, (...data) => this.logger.log({
      type: listenerName,
      context: 'setPageListener',
      ...data,
      ...args
    }))
  },
  /**
   * setUp
   * creates the logger
   * creates the browser instance
   * creates the page instance
   * @returns {Promise<void>}
   */
  async setUp () {
    try {
      this.logger = log.init()
      this.puppeteer = puppeteer
      this.browser = await puppeteer.launch(this.config.launchConfig)
      this.page = await this.browser.newPage()
    } catch (error) {
      this.logger.log({
        name: STATUS.GOTO,
        context: 'setUp',
        url: this.startUrl,
        message: error.toString()
      })
      throw error
    }
  },
  /**
   * getMasterImagePath
   * @param {ImageData} imageData
   * @returns {string}
   */
  getMasterImagePath (imageData) {
    return `${this.config.compareImageMasterPath}/${imageData.sizeDir}/${imageData.baseName}`
  }
}
