/*
* mergeOnlyValues
* Takes 2 objects and creates a new object, target merges with source
* If the target has a null value - then use the source value
* If the target and source both contain null - then use null
* */
const mergeOnlyValues = (source, target) => ({
  ...source,
  ...Object.keys(target).reduce((obj, key) => {
    if (target[key] !== null && target[key] !== undefined) {
      obj[key] = target[key]
    }

    return obj
  }, {})
})

module.exports = mergeOnlyValues
