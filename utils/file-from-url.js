module.exports = {
  /**
   * encode
   * @param {string} string
   * @returns {string}
   */
  encode (string) {
    return string
      .replace(/(^\w+:|^)\/\//, '')
      .replace(/\//g, '!')
      .replace(/:/g, '*')
  },
  /**
   * encode
   * @param {string} string
   * @returns {string}
   */
  decode (string) {
    return 'http://' + string.replace(/!/g, '/').replace('*', ':')
  }
}
