const resizeWindow = async (page, browser, dimmensions) => {
  let { height, width } = dimmensions

  await page.setViewport({ height, width })

  // Window frame - probably OS and WM dependent.
  height += 85

  // Any tab.
  const { targetInfos: [{ targetId }] } = await browser._connection.send(
    'Target.getTargets'
  )
  // Tab window.
  const { windowId } = await browser._connection.send(
    'Browser.getWindowForTarget',
    { targetId }
  )
  // Resize.
  await browser._connection.send('Browser.setWindowBounds', {
    bounds: { height, width },
    windowId
  })

  // scroll the page and add a small amount of timeout
  await page.evaluate(() => {
    return new Promise((resolve) => {
      window.requestAnimationFrame(resolve)
    })
  })
}

module.exports = resizeWindow
