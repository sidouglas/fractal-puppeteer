const console = require('console')
const winston = require('winston')
const lodash = require('lodash')
const readline = require('readline')
const fs = require('fs-extra')
const listDirectory = require('./list-directory')
const STATUS = require('../constants').status

const date = new Date().toISOString().slice(0, 19).replace(/T|:/g, '-')
const currentLogFile = `./log/${date}.log`

const defaults = {
  console,
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'fractal-regression' },
  transports: [
    new winston.transports.File(
      {
        json: false,
        filename: currentLogFile
      }
    )
  ]
}

module.exports = {
// const temp = {
  console (message, type = 'log') {
    const flag = Object.keys(this.options.console).filter(n => n.indexOf(type.toLowerCase()) > -1)
    this.options.console[flag](message)
  },
  async getLog (logPath) {
    const newestLogFile = listDirectory(logPath).pop()
    try {
      return await this.parseLog(newestLogFile.path)
    } catch (error) {
      this.options.console(`could not read ${newestLogFile}. ${error.toString()}`, 'UNREADABLE_FILE')
      throw error
    }
  },
  init (options = {}) {
    this.options = lodash.merge(defaults, options)
    this._logger = winston.createLogger(this.options)
    return this
  },
  log (message) {
    let level = STATUS.ERROR
    if (message && message.name) {
      level = STATUS[message.name] || message.name || level
      delete message.name
      this._logger.log({ level, message })
      this.console(message)
    }
  },
  async parseLog (file) {
    const log = []

    const readLine = await readline.createInterface({
      input: fs.createReadStream(file)
    })

    for await (const line of readLine) {
      log.push(JSON.parse(line))
    }

    return log
  }
}
