const fs = require('fs-extra')
const path = require('path')
const fileFromUrl = require('./file-from-url')

/**
 *
 * @param config
 * @return {ImageData}
 */
module.exports = async (config) => {
  const { page, url, screenSize, baseImagePath } = config

  /*
  *@type {ScreenSize}
  **/
  const { s, x } = screenSize

  const size = `${s}x${x}`

  await fs.ensureDir(`${baseImagePath}/${size}/`)

  const imagePath = `${baseImagePath}/${size}/${fileFromUrl.encode(url)}.png`

  await page.screenshot({
    path: imagePath,
    fullPage: true
  })

  const sizePath = path.dirname(imagePath)
  const branchPath = imagePath.split(path.basename(sizePath))[0]

  /**
   * @typedef {Object} ImageData
   * @property {string} baseName
   * @property {string} branchPath
   * @property {number} height
   * @property {string} path
   * @property {string} sizeDir
   * @property {string} sizePath
   * @property {number} width
   * @type {ImageData}
   */
  return {
    baseName: path.basename(imagePath),
    branchPath,
    height,
    path: imagePath,
    sizeDir: path.basename(sizePath),
    sizePath,
    width
  }
}

screenShot
