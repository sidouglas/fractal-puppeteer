const branch = require('git-branch')
const kebabCase = require('kebab-case')

module.exports = {
  branch: null,
  name (filePath = '.') {
    return branch.sync(filePath)
  },
  init (fullPathbranchWorkingDirectory = '.', prepend = './') {
    this.branch = this.name(fullPathbranchWorkingDirectory)
    this.path = prepend + kebabCase(this.branch.toLowerCase())
  }
}
