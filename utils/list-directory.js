const fs = require('fs')
const trailingSlashIt = require('./trailing-slash-it')

module.exports = (input) => {
  const path = trailingSlashIt(input)
  let files = fs.readdirSync(path)
  let filesWithStats = []
  if (files.length > 1) {
    let sorted = files.sort((a, b) => {
      let s1 = fs.statSync(path + a)
      let s2 = fs.statSync(path + b)
      return s1.ctime < s2.ctime
    })
    sorted.forEach(file => {
      filesWithStats.push({
        filename: file,
        date: new Date(fs.statSync(path + file).ctime),
        path: path + file
      })
    })
  } else {
    files.forEach(file => {
      filesWithStats.push({
        filename: file,
        date: new Date(fs.statSync(path + file).ctime),
        path: path + file
      })
    })
  }
  return filesWithStats
}
