/* {
  emerg: 0,
  alert: 1,
  crit: 2,
  error: 3,
  warning: 4,
  notice: 5,
  info: 6,
  debug: 7
}
*/
module.exports = {
  status: {
    ERROR: 'error',
    FAIL: 'error',
    NOT_FOUND: 'warn',
    PASS: 'info',
    TIMEOUT: 'warn',
    GOTO: 'error',
    UNREADABLE_FILE: 'error'
  }
}
